local title_screen = {}
local main_surface = sol.surface.create()
local title_graphic = sol.surface.create("menus/title.png")
local press_start = sol.sprite.create("menus/press_start")
-- Hide "Press Start" initially
press_start:set_xy(-100, -100)

function title_screen:on_started()
  sol.audio.play_music("alex/rush")
  main_surface:fade_in(85, function()
    -- Show "Press Start" after the fade in
    press_start:set_xy(0, 0)
  end)
end

function title_screen:on_draw(screen)
  screen:clear()
  main_surface:draw(screen)
  title_graphic:draw(main_surface)
  press_start:draw(main_surface, 96, 64)
end

function title_screen:on_key_pressed(key)
  sol.menu.stop(title_screen)
end

return title_screen