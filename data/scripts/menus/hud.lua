-- Displays the HUD


local function initialize_hud(game)

  local items_sprite = sol.sprite.create("entities/items")

  -- Display the item icon in the corner of the screen
  function game:on_draw(dst_surface)

    local item_box = sol.surface.create(20, 20)
    local item_frame = sol.sprite.create("hud/hud")
    item_box:draw(dst_surface, 4, 4)
    item_frame:draw(item_box)

    local trade_variant = game:get_item("trade"):get_variant()

    if trade_variant > 0 then
      items_sprite:set_animation("trade")
      items_sprite:set_direction(trade_variant - 1)
      items_sprite:draw(item_box, 10, 6)
    end

  end

end

-- Set up the HUD on any game that starts.
local game_meta = sol.main.get_metatable("game")
game_meta:register_event("on_started", initialize_hud)

return true