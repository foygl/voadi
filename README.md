# Vegan on a Desert Island

[![Chat with us!](https://img.shields.io/badge/chat%20with%20us!-%23voadi%3Amatrix.org-brightgreen.svg)](https://riot.im/app/#/room/#voadi:matrix.org)

![Screenshot](screenshot.gif)

This game is a work-in-progress 2D top-down adventure game built with [Solarus](http://www.solarus-games.org/).
Unlike most Solarus games, this game is a very simple trading sequence and features no combat.

Inspired by the pervasive question "what would you (a vegan) do if stranded on a desert island?" *Vegan on a Desert Island* seeks to provide an answer.

# Running the game

1. Download [Solarus](http://www.solarus-games.org/).
2. Clone this repo.
3. Open Solarus Quest Editor and click "File > Load Quest..." and select the repo directory.
4. Click the blue play button at the top.
5. That's it!

Alternatively, you can run `solarus-run /path/to/game` from the command line.

# License

![GPL](https://www.gnu.org/graphics/gplv3-127x51.png)

Vegan on a Desert Island is Free Software, licensed under GPL-3.0.
See the `LICENSE` file included in this repo for a copy of the full license.

Game assets are licensed under a Free Culture Approved Creative Commons license.
For now, a working list is available under `attributions.txt` included in this repo.
The final version of the game will credit the authors within the game itself.
