-- Lua script of map forest.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map becomes is loaded.
function map:on_started()

  -- You can initialize the movement and sprites of various
  -- map entities here.
  local mimosa = map:get_entity("mimosa")
  local trade_variant = game:get_item("trade"):get_variant()

  -- Set to brown if not watered
  if trade_variant < 2 then
    mimosa:get_sprite():set_animation("brown")
  end

  -- Mimosa interaction override
  function mimosa:on_interaction()
    local trade_variant = game:get_item("trade"):get_variant()
    if trade_variant < 2 then
      -- Plant hasn't been watered
      game:start_dialog("npc.mimosa.water", function(choice)
        local trade_variant = game:get_item("trade"):get_variant()

        if choice == 1 and trade_variant == 1 then
          -- Make it green
          game:get_item("trade"):set_variant(0)
          mimosa:get_sprite():set_animation("green")
          -- Start dialog
          game:start_dialog("npc.mimosa.water.yes_hasitem", function()
             -- Get vitamin B12
            hero:start_treasure("trade", 2)
          end)

        elseif choice == 1 then
          game:start_dialog("npc.mimosa.water.yes_noitem")

        else
          game:start_dialog("npc.mimosa.water.no")

        end

      end)

    else
      -- Plant has been watered
      game:start_dialog("npc.mimosa.water.watered")
    end

  end

end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end
