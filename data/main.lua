-- This is the main Lua script of your project.
-- You will probably make a title screen and then start a game.
-- See the Lua API! http://www.solarus-games.org/doc/latest

require("scripts/features")
local game_manager = require("scripts/game_manager")
local solarus_logo = require("scripts/menus/solarus_logo")
local title_screen = require("scripts/menus/title_screen")

-- This function is called when Solarus starts.
function sol.main:on_started()

  -- Setting a language is useful to display text and dialogs.
  sol.language.set_language("en")

  local program = self

  -- Show the Solarus logo initially.
  sol.menu.start(program, solarus_logo)

  -- Title screen after Solarus logo
  function solarus_logo:on_finished()
    sol.menu.start(program, title_screen)
  end

  -- Start the game when the title screen is finished.
  function title_screen:on_finished()
    local game = game_manager:create("save1.dat")
    sol.main:start_savegame(game)
  end

end

-- Event called when the player pressed a keyboard key.
function sol.main:on_key_pressed(key, modifiers)

  local handled = false
  if key == "f11" or
    (key == "return" and (modifiers.alt or modifiers.control)) then
    -- F11 or Ctrl + return or Alt + Return: switch fullscreen.
    sol.video.set_fullscreen(not sol.video.is_fullscreen())
    handled = true
  elseif key == "f4" and modifiers.alt then
    -- Alt + F4: stop the program.
    sol.main.exit()
    handled = true
  elseif key == "escape" and sol.main.game == nil then
    -- Escape in title screens: stop the program.
    sol.main.exit()
    handled = true
  elseif key == "escape" and sol.video.is_fullscreen() then
    -- Escape fullscreen
    sol.video.set_fullscreen(false)
    handled = true
  end

  return handled
end

-- Starts a game.
function sol.main:start_savegame(game)

  -- Skip initial menus if any.
  sol.menu.stop(solarus_logo)

  sol.main.game = game

  -- Prevent attack from being held; no tapping or spin attacks
  function sol.main.game:on_command_pressed(command)
    if command == "attack" then
      sol.main.game:get_hero():start_attack()
      sol.main.game:simulate_command_released("attack")
      return false
    end
  end

  game:start()
end
