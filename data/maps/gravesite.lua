-- Lua script of map gravesite.
-- This script is executed every time the hero enters this map.

-- Feel free to modify the code below.
-- You can add more events and remove the ones you don't need.

-- See the Solarus Lua API documentation:
-- http://www.solarus-games.org/doc/latest

local map = ...
local game = map:get_game()

-- Event called at initialization time, as soon as this map becomes is loaded.
function map:on_started()

  -- You can initialize the movement and sprites of various
  -- map entities here.

  local ocean = map:get_entity("ocean")

  ocean:set_traversable_by("hero", false)

  function ocean:on_interaction()
    game:start_dialog("ocean.grave")
  end

  local gravestone = map:get_entity("gravestone")

  function gravestone:on_interaction()

    game:start_dialog("gravestone", function(status)

      if game:get_item("trade"):get_variant() < 1 then
        -- Get male tears
        hero:start_treasure("trade", 1)
      end

    end)

  end

end

-- Event called after the opening transition effect of the map,
-- that is, when the player takes control of the hero.
function map:on_opening_transition_finished()

end
